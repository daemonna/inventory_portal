use std::io::prelude::*;
//use std::fs;
use std::net::TcpListener;
use std::net::TcpStream;
mod root;
mod inventory;
mod html;

fn main() {
    println!("rustie http://127.0.0.1:7878");
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    //let pool = ThreadPool::new(4);

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream);
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();

    let root = b"GET / HTTP/1.1\r\n";
    let inventory = b"GET /inv HTTP/1.1\r\n";
    let response;

    if buffer.starts_with(root) {
        response = root::get_root();
    } else if buffer.starts_with(inventory) {
        response = inventory::get_inventory();
    }  else {
        let status_line = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
        let contents = "error 404 NOT FOUND"; //fs::read_to_string("404.html").unwrap();
        //contents = html::get_html();
        response = format!("{}{}", status_line, contents);
    }

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}